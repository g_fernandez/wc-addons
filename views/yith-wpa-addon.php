<?php
$loop = isset( $args['loop'] ) ? $args['loop'] : 0;
//error_log(print_r('addons single', true));
//error_log(print_r($args,true));
?>

<div class="yith_wpa_container">

	<div class="yith_wpa_head">
		<div>
			<span class="dashicons dashicons-arrow-up-alt2 yith_wpa_head_arrow"></span>
			<span class="yith_wpa_head_name"><?php echo esc_attr( $args['addons']['name'] ); ?></span>
		</div>

		<label class="switch">
			<input
				type="checkbox"
				name="yith_wpa_addon[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][enabled]"
				value="yes"
				id="yith_wpa_enabled_{{INDEX}}"
				<?php checked( $args['addons']['enabled'], 'yes' ); ?>
			>
			<span class="slider round"></span>
		</label>
	</div>


	<div class="yith_wpa_body">
		<input type="hidden" name="yith_wpa_addon[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][index]" class="yith_wpa_addon_index" value="<?php echo '' !== $args['addons']['index'] ? esc_attr( $args['addons']['index'] ) : ''; ?>">

		<p class="form-field yith_wpa_name_input">
			<label for="yith_wpa_name_{{INDEX}}"><?php esc_html_e( 'Name', 'yith-plugin-addons' ); ?></label>
			<input
				type="text"
				id="yith_wpa_name_{{INDEX}}"
				name="yith_wpa_addon[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][name]"
				value="<?php echo '' !== $args['addons']['name'] ? esc_attr( $args['addons']['name'] ) : ''; ?>"
			>
			<br><span class="yith_wpa_input_description"><?php esc_html_e( 'The name of the Add-on', 'yith-plugin-addons' ); ?></span>
		</p>

		<p class="form-field yith_wpa_description_textarea">
			<label for="yith_wpa_description_{{INDEX}}"><?php esc_html_e( 'Description', 'yith-plugin-addons' ); ?></label>
			<textarea id="yith_wpa_description_{{INDEX}}" name="yith_wpa_addon[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][description]" rows="4" cols="50"><?php echo '' !== $args['addons']['description'] ? esc_attr( $args['addons']['description'] ) : ''; ?></textarea>
			<br><span class="yith_wpa_input_description"><?php esc_html_e( 'Set here the description that will be shown above the add-on field', 'yith-plugin-addons' ); ?></span>
		</p>

		<p class="form-field yith_wpa_field_type_input">

			<label for="yith_wpa_field_type_{{INDEX}}"><?php esc_html_e( 'Field Type', 'yith-plugin-addons' ); ?></label>
			<select name="yith_wpa_addon[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][field_type]" id="yith_wpa_field_type_{{INDEX}}">
				<option value="text" <?php echo 'text' === $args['addons']['field_type'] ? 'selected' : ''; ?>><?php esc_html_e( 'Text', 'yith-plugin-addons' ); ?></option>
				<option value="textarea" <?php echo 'textarea' === $args['addons']['field_type'] ? 'selected' : ''; ?>><?php esc_html_e( 'Textarea', 'yith-plugin-addons' ); ?></option>
				<option value="select" <?php echo 'select' === $args['addons']['field_type'] ? 'selected' : ''; ?>><?php esc_html_e( 'Select', 'yith-plugin-addons' ); ?></option>
				<option value="radio" <?php echo 'radio' === $args['addons']['field_type'] ? 'selected' : ''; ?>><?php esc_html_e( 'Radio', 'yith-plugin-addons' ); ?></option>
				<option value="checkbox" <?php echo 'checkbox' === $args['addons']['field_type'] ? 'selected' : ''; ?>><?php esc_html_e( 'Checkbox', 'yith-plugin-addons' ); ?></option>
				<option value="onoff" <?php echo 'onoff' === $args['addons']['field_type'] ? 'selected' : ''; ?>><?php esc_html_e( 'On/Off', 'yith-plugin-addons' ); ?></option>
			</select>
			<br><span class="yith_wpa_input_description"><?php esc_html_e( 'Choose the type of the add-on field', 'yith-plugin-addons' ); ?></span>

		</p>

		<fieldset class="form-field yith_wpa_price_settings_input">
			<legend><?php echo esc_html__( 'Price Settings', 'yith-plugin-addons' ); ?></legend>


			<ul class="wc-radios">
				<li class="yith_wpa_free">
					<label>
						<input type="radio" id="yith_wpa_free_{{INDEX}}" name="yith_wpa_addon[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][price_settings]" value="free" <?php checked( $args['addons']['price_settings'], 'free' ); ?>>
						<?php echo esc_html__( 'Free', 'yith-plugin-addons' ); ?>
					</label>
				</li>
				<li class="yith_wpa_fixed_price">
					<label>
						<input type="radio" id="yith_wpa_fixed_price_{{INDEX}}" name="yith_wpa_addon[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][price_settings]" value="fixed_price" <?php checked( $args['addons']['price_settings'], 'fixed_price' ); ?>>
						<?php echo esc_html__( 'Fixed Price', 'yith-plugin-addons' ); ?>
					</label>
				</li>
				<li class="yith_wpa_price_per_char">
					<label>
						<input type="radio" id="yith_wpa_price_per_character_{{INDEX}}" name="yith_wpa_addon[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][price_settings]" value="price_per_character" <?php checked( $args['addons']['price_settings'], 'price_per_character' ); ?>>
						<?php echo esc_html__( 'Price per character', 'yith-plugin-addons' ); ?>
					</label>
				</li>
			</ul>
			<span class="yith_wpa_input_description"><?php esc_html_e( 'Choose how the add-on will influence the product price', 'yith-plugin-addons' ); ?></span>
		</fieldset>

		<fieldset class="form-field yith_wpa_options_input">
			<legend><?php esc_html_e( 'Options', 'yith-plugin-addons' ); ?></legend>

			<span class="yith_wpa_option_container_total">
				<?php
				foreach ( $args['addons']['options']['name'] as $key => $option_name ) {
					?>
					<span class="yith_wpa_option_container">
						<span class="yith_wpa_option_container_name">
							<label for="yith_wpa_option_name">Name</label>
							<input
								type="text"
								class="yith_wpa_option_name"
								name="yith_wpa_addon[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][options][name][]"
								value="<?php echo '' !== $args['addons']['options']['name'][ $key ] ? esc_attr( $args['addons']['options']['name'][ $key ] ) : ''; ?>"
							>
						</span>
						<span class="yith_wpa_option_container_price">
							<label for="yith_wpa_option_price" >Price</label>
							<input
								type="text"
								class="yith_wpa_option_price"
								name="yith_wpa_addon[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][options][price][]"
								value="<?php echo '' !== $args['addons']['options']['price'][ $key ] ? esc_attr( $args['addons']['options']['price'][ $key ] ) : ''; ?>"
							>
						</span>
						<?php
						if ( 0 === $key ) {
							?>
							<span class="dashicons dashicons-trash yith_wpa_option_delete" style="display: none"></span>
							<?php
						} else {
							?>
							<span class="dashicons dashicons-trash yith_wpa_option_delete"></span>
							<?php
						}
						?>
					</span>
					<?php
				}
				?>
			</span>


			<br><span style="display:inline-block" class="yith_wpa_add_new_option"><?php esc_html_e( '+ Add new option', 'yith-plugin-addons' ); ?></span>
			<br><span class="yith_wpa_input_description"><?php esc_html_e( 'Set the options for the add-on', 'yith-plugin-addons' ); ?></span>
		</fieldset>

		<p class="form-field yith_wpa_price_input">
			<label for="yith_wpa_price_{{INDEX}}"><?php esc_html_e( 'Price', 'yith-plugin-addons' ); ?></label>
			<input
				type="text"
				id="yith_wpa_price_{{INDEX}}"
				name="yith_wpa_addon[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][price]"
				value="<?php echo '' !== $args['addons']['price'] ? esc_attr( $args['addons']['price'] ) : ''; ?>"
			>
			<br><span class="yith_wpa_input_description"><?php esc_html_e( 'Choose the price of the add-on', 'yith-plugin-addons' ); ?></span>
		</p>

		<p class="form-field yith_wpa_free_characters_input">
			<label for="yith_wpa_free_characters_{{INDEX}}"><?php esc_html_e( 'Free characters', 'yith-plugin-addons' ); ?></label>
			<input
				type="number"
				id="yith_wpa_free_characters_{{INDEX}}"
				name="yith_wpa_addon[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][free_characters]"
				value="<?php echo '' !== $args['addons']['free_characters'] ? esc_attr( $args['addons']['free_characters'] ) : ''; ?>"
			>
			<br><span class="yith_wpa_input_description"><?php esc_html_e( 'Choose the number of free characters', 'yith-plugin-addons' ); ?></span>
		</p>

		<p class="form-field yith_wpa_default_enabled_input">
			<label for="yith_wpa_default_enabled"><?php esc_html_e( 'Default Enabled', 'yith-plugin-addons' ); ?></label>
			<label class="switch">
				<input
					type="checkbox"
					name="yith_wpa_addon[<?php echo esc_attr( $loop ); ?>][{{INDEX}}][default_enabled]"
					value="yes"
					id="yith_wpa_default_enabled_{{INDEX}}"
					<?php
					if ( isset( $args['addons']['default_enabled'] ) ) {
						checked( $args['addons']['default_enabled'], 'yes' );
					}
					?>
				>
				<span class="slider round"></span>
			</label>
			<br><span class="yith_wpa_input_description"><?php esc_html_e( 'Enabled if the field is enabled by default', 'yith-plugin-addons' ); ?></span>
		</p>

		<span class="yith_wpa_delete_addon"><?php esc_html_e( 'REMOVE ADDON', 'yith_plugin_addons' ); ?></span>

	</div>

</div>
