<?php
$default_fields_values = array(
	'enabled'         => 'yes',
	'name'            => __( 'Unnamed', 'yith_plugin_addons' ),
	'description'     => __( 'Description', 'yith_plugin_addons' ),
	'field_type'      => 'text',
	'price_settings'  => 'free',
	'price'           => '0',
	'free_characters' => 0,
	'options'         => array(
		'name'  => array(
			0 => '',
		),
		'price' => array(
			0 => '',
		),
	),
	'default_enabled' => 'no',
	'index'           => 0,
	'loop'            => 0,
);

$default_args = array( 'addons' => $default_fields_values );
if ( isset( $args['loop'] ) ) {
	$default_args['loop'] = $args['loop'] + 1;
}
//error_log(print_r('args conteiner', true));
//error_log(print_r($args,true));
?>
<div class="yith_wpa_container_total">
	<span class="yith_wpa_add_new"><?php esc_html_e( 'ADD NEW ADD-ON', 'yith_plugin_addons' ); ?></span>

	<div class="yith_wpa_template_hidden">
		<?php yith_wpa_get_view( '/yith-wpa-addon.php', $default_args ); ?>
	</div>

	<div class="yith_wpa_addons">
		<?php
		foreach ( $args['addons'] as $addon ) {
			$args_addon = array( 'addons' => $addon );
			if ( isset( $args['loop'] ) ) {
				$args_addon['loop'] = $args['loop'] + 1;
			}
			yith_wpa_get_view( '/yith-wpa-addon.php', $args_addon );
		}
		?>
	</div>
</div>
