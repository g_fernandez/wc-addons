<?php
/**
 * YITH Product Addons
 *
 * @package wc-addons
 */

if ( ! defined( 'YITH_WPA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_WPA_Ajax' ) ) {

	/**
	 * YITH_WPA_Ajax
	 */
	class YITH_WPA_Ajax {

		/**
		 * Main Instance
		 *
		 * @var $instance
		 */
		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return YITH_WPA_Ajax Main instance.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'wp_enqueue_scripts', array( $this, 'yith_wpa_enqueue_script_ajax' ) );
			add_action( 'wp_ajax_yith_wpa_change_variations_addons', array( $this, 'yith_wpa_change_variations_addons' ) );
			add_action( 'wp_ajax_nopriv_yith_wpa_change_variations_addons', array( $this, 'yith_wpa_change_variations_addons' ) );
		}

		/**
		 * Ajax Scripts
		 */
		public function yith_wpa_enqueue_script_ajax() {
			if ( function_exists( 'is_product' ) && is_product() ) {
				wp_register_script( 'ajax_script', YITH_WPA_DIR_ASSETS_JS_URL . '/yith-wpa-ajax.js', array( 'jquery' ), YITH_WPA_VERSION, false );
				wp_localize_script( 'ajax_script', 'myAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' ) ) );
				wp_enqueue_script( 'jquery' );
				wp_enqueue_script( 'ajax_script' );

				wp_localize_script(
					'ajax_script',
					'yithWcpa',
					array(
						'decimalSeparator' => wc_get_price_decimal_separator(),
						'decimalPrecision' => wc_get_price_decimals(),
						'productPrice'     => wc_get_product()->get_price(),
					)
				);
			}
		}

		/**
		 * Change Note Price
		 */
		public function yith_wpa_change_variations_addons() {
			$currency = get_woocommerce_currency_symbol();
			if ( isset( $_POST['start_id'] ) ) {
				$start_id   = $_POST['start_id'];
				$start_cant = $_POST['start_id'];
			}

			if ( 0 === (int) $start_cant ) {
				$exists = false; // No general addons.
			} else {
				$exists = true; // Exists at least one general addon.
			}
			ob_start();
			if ( isset( $_POST['variation_id'] ) ) {
				$variation_id = intval( $_POST['variation_id'] );
				$variation    = wc_get_product( $variation_id );

				$addons = $variation->get_meta( 'yith_wpa_addons' );

				foreach ( $addons as $addon ) {
					$addon['index'] = $start_id;
					$start_id++;

					$args = array(
						'addon'    => $addon,
						'currency' => $currency,
					);
					if ( 'yes' === $addon['enabled'] ) {
						echo '<div class="yith_wpa_addon">';
						wc_get_template( '/addon-fields/' . $addon['field_type'] . '.php', $args, '', trailingslashit( YITH_WPA_DIR_TEMPLATES_PATH ) );
						echo '</div>';
						if ( ! $exists ) {
							$create_addon_price = true;
						}
					}
				}
				if ( isset( $create_addon_price ) && $create_addon_price ) {
					$args = array(
						'currency'      => $currency,
						'product_price' => $variation->get_price(),
					);
					wc_get_template( '/yith-wpa-product-prices.php', $args, '', trailingslashit( YITH_WPA_DIR_TEMPLATES_PATH ) );
				}
			}

			echo wp_json_encode( array( 'addons' => ob_get_clean() ) );
			die();
		}

	}
}
