<?php
/**
 * YITH Product Addons
 *
 * @package wc-addons
 */

if ( ! defined( 'YITH_WPA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_WPA_Frontend' ) ) {

	/**
	 * YITH_WCN_Frontend
	 */
	class YITH_WPA_Frontend {

		/**
		 * YITH_WCN_Frontend instance.
		 *
		 * @var YITH_WPA_Frontend
		 */
		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return YITH_WPA_Frontend Main instance.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'wp_enqueue_scripts', array( $this, 'yith_wpa_enqueue_scripts' ) );
			add_action( 'woocommerce_before_add_to_cart_button', array( $this, 'yith_wpa_add_addons_product_page' ) );

			add_filter( 'woocommerce_add_cart_item_data', array( $this, 'yith_wpa_add_cart_item_data' ), 10, 3 );
			add_action( 'woocommerce_before_calculate_totals', array( $this, 'yith_wpa_add_custom_price' ), 10, 1 );
			add_filter( 'woocommerce_get_item_data', array( $this, 'yith_wpa_product_add_on_display_cart' ), 10, 2 );
			add_action( 'woocommerce_add_order_item_meta', array( $this, 'yith_wpa_product_add_order_item_meta' ), 10, 2 );
			add_filter( 'woocommerce_order_item_product', array( $this, 'yith_wpa_product_add_on_display_order' ), 10, 2 );

		}

		/**
		 * Enqueue frontend scripts
		 */
		public function yith_wpa_enqueue_scripts() {

			wp_register_style( 'yith-wpa-wp-css', YITH_WPA_DIR_ASSETS_CSS_URL . '/yith-wpa-frontend.css', array(), YITH_WPA_VERSION );
			wp_register_script( 'yith-wpa-wp-js', YITH_WPA_DIR_ASSETS_JS_URL . '/yith-wpa-frontend.js', array( 'jquery' ), YITH_WPA_VERSION, false );

			if ( function_exists( 'is_product' ) && is_product() ) {
				wp_enqueue_style( 'yith-wpa-wp-css' );
				wp_enqueue_script( 'yith-wpa-wp-js' );

				wp_localize_script(
					'yith-wpa-wp-js',
					'yithWcpa',
					array(
						'decimalSeparator' => wc_get_price_decimal_separator(),
						'decimalPrecision' => wc_get_price_decimals(),
						'productPrice'     => wc_get_product()->get_price(),
					)
				);
			}
		}

		/**
		 * Add all addons from a product meta in product page
		 */
		public function yith_wpa_add_addons_product_page() {

			global $product;
			$addons   = get_post_meta( $product->get_id(), 'yith_wpa_addons' );
			$currency = get_woocommerce_currency_symbol();
			$exists   = false;
			echo '<div class="yith_wpa_addons_container">';
			echo '<div class="yith_wpa_variation_addons"></div>';
			foreach ( $addons[0] as $addon ) {

				$args = array(
					'addon'    => $addon,
					'currency' => $currency,
				);

				if ( 'yes' === $addon['enabled'] ) {
					echo '<div class="yith_wpa_addon">';
					wc_get_template( '/addon-fields/' . $addon['field_type'] . '.php', $args, '', trailingslashit( YITH_WPA_DIR_TEMPLATES_PATH ) );
					echo '</div>';
					$exists = true;
				}
			}
			if ( ! empty( $addons ) && $exists ) {
				$args = array(
					'currency'      => $currency,
					'product_price' => $product->get_price(),
				);
				wc_get_template( '/yith-wpa-product-prices.php', $args, '', trailingslashit( YITH_WPA_DIR_TEMPLATES_PATH ) );
			}
			echo '</div>';

		}

		/**
		 * Save addons input value into cart item data.
		 *
		 * @param Array $cart_item_data Product item.
		 * @param int   $product_id Product ID.
		 */
		public function yith_wpa_add_cart_item_data( $cart_item_data, $product_id, $variation_id ) {
			$addons   = get_post_meta( $product_id, 'yith_wpa_addons' );
			$currency = get_woocommerce_currency();

			$variation  = wc_get_product( $variation_id );
			$addons_var = $variation->get_meta( 'yith_wpa_addons' );
			$index      = 0;

			if ( ! empty( $addons ) ) {
				foreach ( $addons[0] as $addon ) {
					if ( ! empty( $_POST[ 'yith_wpa_field_' . $addon['index'] ] ) ) {
						$cart_item_data[ 'yith_wpa_field_' . $addon['index'] ] = sanitize_text_field( wp_unslash( $_POST[ 'yith_wpa_field_' . $addon['index'] ] ) );
					}
					$index ++;
				}
			}

			if ( ! empty( $addons_var ) ) {
				foreach ( $addons_var as $addon ) {
					if ( ! empty( $_POST[ 'yith_wpa_field_' . $index ] ) ) {
						$cart_item_data[ 'yith_wpa_field_' . $index ] = sanitize_text_field( wp_unslash( $_POST[ 'yith_wpa_field_' . $index ] ) );
					}
					$index ++;
				}
			}

			if ( ! empty( $_POST['yith_wpa_price_totals_hidden'] ) ) {
				$cart_item_data['yith_wpa_total_price'] = sanitize_text_field( wp_unslash( $_POST['yith_wpa_price_totals_hidden'] ) );
			}

			return $cart_item_data;
		}

		/**
		 * Set new price to products with custom note.
		 *
		 * @param Object $cart Cart.
		 */
		public function yith_wpa_add_custom_price( $cart ) {
			foreach ( $cart->get_cart() as $hash => $value ) {
				if ( ! empty( $value['yith_wpa_total_price'] ) ) {
					$value['data']->set_price( (float) $value['yith_wpa_total_price'] );
				}
			}
		}

		/**
		 * Display addons on cart.
		 *
		 * @param Object $item_data item data.
		 * @param Object $cart_item cart item.
		 */
		public function yith_wpa_product_add_on_display_cart( $item_data, $cart_item ) {

			$addons      = get_post_meta( $cart_item['product_id'], 'yith_wpa_addons' );
			$_product    = wc_get_product( $cart_item['product_id'] );
			$currency    = get_woocommerce_currency_symbol();
			$index       = 0;
			$item_data[] = array(
				'name'  => __( 'Base Price', 'yith-plugin-addons' ),
				'value' => $currency . $_product->get_price(),
			);
			foreach ( $addons[0] as $addon ) {
				$item_data = $this->yith_wpa_add_addon_to_item_data( $currency, $cart_item, $addon, $item_data, $index );
				$index ++;
			}

			$variation_id = intval( $cart_item['variation_id'] );
			$variation    = wc_get_product( $variation_id );
			$addons       = $variation->get_meta( 'yith_wpa_addons' );
			foreach ( $addons as $addon ) {
				$item_data = $this->yith_wpa_add_addon_to_item_data( $currency, $cart_item, $addon, $item_data, $index );
				$index ++;
			}
//			error_log(print_r('item data suma',true));
//			error_log(print_r($item_data,true));
			return $item_data;
		}

		function yith_wpa_add_addon_to_item_data( $currency, $cart_item, $addon, $item_data, $index ) {
			if ( ! empty( $cart_item[ 'yith_wpa_field_' . $index ] ) ) {
				if ( 'text' === $addon['field_type'] || 'textarea' === $addon['field_type'] ) {
					if ( 'free' === $addon['price_settings'] ) {
						$item_data[] = array(
							'name'  => $addon['name'],
							'value' => sanitize_text_field( $cart_item[ 'yith_wpa_field_' . $index ] ),
						);
					} elseif ( 'fixed_price' === $addon['price_settings'] ) {
						if ( $addon['free_characters'] < strlen( sanitize_text_field( $cart_item[ 'yith_wpa_field_' . $index ] ) ) ) {
							$item_data[] = array(
								'name'  => $addon['name'] . '(+' . $currency . $addon['price'] . ')',
								'value' => sanitize_text_field( $cart_item[ 'yith_wpa_field_' . $index ] ),
							);
						} else {
							$item_data[] = array(
								'name'  => $addon['name'],
								'value' => sanitize_text_field( $cart_item[ 'yith_wpa_field_' . $index ] ),
							);
						}
					} elseif ( 'price_per_character' === $addon['price_settings'] ) {
						if ( $addon['free_characters'] < strlen( sanitize_text_field( $cart_item[ 'yith_wpa_field_' . $index ] ) ) ) {
							$price       = (int) $addon['price'] * ( strlen( sanitize_text_field( $cart_item[ 'yith_wpa_field_' . $index ] ) ) - $addon['free_characters'] );
							$item_data[] = array(
								'name'  => $addon['name'] . '(+' . $currency . $price . ')',
								'value' => sanitize_text_field( $cart_item[ 'yith_wpa_field_' . $index ] ),
							);
						} else {
							$item_data[] = array(
								'name'  => $addon['name'],
								'value' => sanitize_text_field( $cart_item[ 'yith_wpa_field_' . $index ] ),
							);
						}
					}
				} elseif ( 'radio' === $addon['field_type'] || 'select' === $addon['field_type'] ) {
					if ( 'free' === $addon['price_settings'] ) {
						$item_data[] = array(
							'name'  => $addon['name'],
							'value' => sanitize_text_field( $cart_item[ 'yith_wpa_field_' . $index ] ),
						);
					} else {
						foreach ( $addon['options']['name'] as $key => $option_name ) {
							if ( ( sanitize_text_field( $cart_item[ 'yith_wpa_field_' . $index ] ) ) === $option_name ) {
								$item_data[] = array(
									'name'  => $addon['name'] . '(+' . $currency . $addon['options']['price'][ $key ] . ')',
									'value' => sanitize_text_field( $cart_item[ 'yith_wpa_field_' . $index ] ),
								);
							}
						}
					}
				} else {
					if ( 'free' === $addon['price_settings'] ) {
						$item_data[] = array(
							'name'  => $addon['name'],
							'value' => sanitize_text_field( $cart_item[ 'yith_wpa_field_' . $index ] ),
						);
					} else {
						$item_data[] = array(
							'name'  => $addon['name'] . '(+' . $currency . $addon['price'] . ')',
							'value' => sanitize_text_field( $cart_item[ 'yith_wpa_field_' . $index ] ),
						);
					}
				}
			}
			return $item_data;
		}

		/**
		 * Add on order item meta
		 *
		 * @param  Int    $item_id Item ID.
		 * @param  Object $values values.
		 * @throws Exception Exception.
		 */
		public function yith_wpa_product_add_order_item_meta( $item_id, $values ) {
			$addons     = get_post_meta( $values['product_id'], 'yith_wpa_addons' );
			$variation  = wc_get_product( $values['variation_id'] );
			$addons_var = $variation->get_meta( 'yith_wpa_addons' );
			$index      = 0;
			foreach ( $addons[0] as $addon ) {
				if ( ! empty( $values[ 'yith_wpa_field_' . $addon['index'] ] ) ) {
					wc_add_order_item_meta( $item_id, $addon['name'], $values[ 'yith_wpa_field_' . $addon['index'] ], true );
				}
				$index ++;
			}
			foreach ( $addons_var as $addon ) {
				if ( ! empty( $values[ 'yith_wpa_field_' . $index ] ) ) {
					wc_add_order_item_meta( $item_id, $addon['name'], $values[ 'yith_wpa_field_' . $index ], true );
				}
				$index ++;
			}
		}

		/**
		 * Add on display order
		 *
		 * @param Object $product item ID.
		 * @param Object $item Item.
		 */
		public function yith_wpa_product_add_on_display_order( $product, $item ) {
			$addons     = get_post_meta( $product->get_id(), 'yith_wpa_addons' );
			$variation  = wc_get_product( $item->get_variation_id() );
			$addons_var = $variation->get_meta( 'yith_wpa_addons' );
			$index      = 0;
			foreach ( $addons[0] as $addon ) {
				if ( ! empty( $item[ 'yith_wpa_field_' . $addon['index'] ] ) ) {
					$product[ 'yith_wpa_field_' . $addon['name'] ] = $item[ 'yith_wpa_field_' . $addon['index'] ];
				}
				$index ++;
			}
			foreach ( $addons_var as $addon ) {
				if ( ! empty( $item[ 'yith_wpa_field_' . $index ] ) ) {
					$product[ 'yith_wpa_field_' . $addon['name'] ] = $item[ 'yith_wpa_field_' . $index ];
				}
				$index ++;
			}
			return $product;
		}
	}
}
