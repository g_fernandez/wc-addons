<?php
/**
 * YITH Product Addons
 *
 * @package wc-addons
 */

if ( ! defined( 'YITH_WPA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_WPA_Plugin_Addons' ) ) {
	/**
	 * YITH_WPA_Plugin_Addons
	 */
	class YITH_WPA_Plugin_Addons {

		/**
		 * Plugin Addons instance.
		 *
		 * @var YITH_WPA_Plugin_Addons
		 */
		private static $instance;

		/**
		 * Admin class instance.
		 *
		 * @var YITH_WPA_Admin
		 */
		public $admin = null;

		/**
		 * Frontend class instance.
		 *
		 * @var YITH_WPA_Frontend
		 */
		public $frontend = null;

		/**
		 * Ajax class instance.
		 *
		 * @var YITH_WPA_Ajax
		 */
		public $ajax = null;

		/**
		 * Get_instance
		 *
		 * @return YITH_WPA_Plugin_Addons Main instance.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();

		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {

			$require = apply_filters(
				'yith_wpa_require_class',
				array(
					'common'   => array(
						'includes/functions.php',
						'includes/class-yith-wpa-ajax.php',
					),
					'admin'    => array(
						'includes/class-yith-wpa-admin.php',
					),
					'frontend' => array(
						'includes/class-yith-wpa-frontend.php',
					),
				)
			);

			$this->_require( $require );

			$this->init_classes();

			$this->init();

		}

		/**
		 * _require
		 *
		 * @param mixed $main_classes Main.
		 * @return void
		 */
		protected function _require( $main_classes ) {
			foreach ( $main_classes as $section => $classes ) {
				foreach ( $classes as $class ) {
					if ( 'common' === $section || ( 'frontend' === $section && ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) || ( 'admin' === $section && is_admin() ) && file_exists( YITH_WPA_DIR_PATH . $class ) ) {
						require_once YITH_WPA_DIR_PATH . $class;
					}
				}
			}
		}

		/**
		 * Init_classes
		 *
		 * @return void
		 */
		public function init_classes() {
			$this->ajax = YITH_WPA_Ajax::get_instance();
		}

		/**
		 * Init
		 *
		 * @return void
		 */
		public function init() {

			if ( is_admin() ) {
				$this->admin = YITH_WPA_Admin::get_instance();
			}

			if ( ! is_admin() || ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
				$this->frontend = YITH_WPA_Frontend::get_instance();
			}

		}

	}

}

if ( ! function_exists( 'yith_wpa_plugin_addons' ) ) {
	/**
	 * Yith_wpa_plugin_addons
	 *
	 * @return YITH_WPA_Plugin_Addons Instance.
	 */
	function yith_wpa_plugin_addons() {
		return YITH_WPA_Plugin_Addons::get_instance();
	}
}
