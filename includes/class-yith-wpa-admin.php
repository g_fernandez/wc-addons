<?php
/**
 * YITH Product Addons
 *
 * @package wc-addons
 */

if ( ! defined( 'YITH_WPA_VERSION' ) ) {
	exit( 'Direct access forbidden.' );
}

if ( ! class_exists( 'YITH_WPA_Admin' ) ) {

	/**
	 * YITH_WPA_Admin
	 */
	class YITH_WPA_Admin {

		/**
		 * Main Instance
		 *
		 * @var $instance
		 */
		private static $instance;

		/**
		 * Get_instance
		 *
		 * @return YITH_WPA_Admin Main instance.
		 */
		public static function get_instance() {
			return ! is_null( self::$instance ) ? self::$instance : self::$instance = new self();
		}

		/**
		 * __construct
		 *
		 * @return void
		 */
		private function __construct() {
			add_action( 'admin_enqueue_scripts', array( $this, 'yith_wpa_enqueue_scripts' ), 10, 0 );
			add_filter( 'woocommerce_product_data_tabs', array( $this, 'yith_wpa_create_addons_tab' ), 10, 1 );
			add_action( 'woocommerce_product_data_panels', array( $this, 'yith_wpa_display_addons_fields' ), 10, 0 );
			add_action( 'woocommerce_process_product_meta', array( $this, 'yith_wpa_save_fields' ), 10, 1 );
			add_action( 'woocommerce_product_after_variable_attributes', array( $this, 'yith_wpa_add_addons_variations' ), 10, 3 );
			add_action( 'woocommerce_admin_process_variation_object', array( $this, 'yith_wpa_save_addons_variations' ), 10, 2 );
		}

		/**
		 * Enqueue backend scripts
		 */
		public function yith_wpa_enqueue_scripts() {
			wp_register_style( 'yith-wpa-admin-css', YITH_WPA_DIR_ASSETS_CSS_URL . '/yith-wpa-backend.css', array(), YITH_WPA_VERSION );
			wp_register_script( 'yith-wpa-admin-js', YITH_WPA_DIR_ASSETS_JS_URL . '/yith-wpa-backend.js', array( 'jquery' ), YITH_WPA_VERSION, false );

			wp_enqueue_style( 'yith-wpa-admin-css' );
			wp_enqueue_script( 'yith-wpa-admin-js' );
		}

		/**
		 * Add the new tab to the $tabs array
		 *
		 * @param array $tabs tabs.
		 */
		public function yith_wpa_create_addons_tab( $tabs ) {
			$tabs['product_addons'] = array(
				'label'    => __( 'Addons', 'yith-plugin-addons' ),
				'target'   => 'product_addons_panel',
				'priority' => 80,
			);
			return $tabs;
		}

		/**
		 * Display fields for the new panel
		 */
		public function yith_wpa_display_addons_fields() {
			global $post;
			$addons = get_post_meta( $post->ID, 'yith_wpa_addons', true );
//			error_log(print_r('addons generales', true));
//			error_log(print_r($addons,true));
			echo '<div id="product_addons_panel" class="panel woocommerce_options_panel">';
			echo '<div class="options_group">';
			yith_wpa_get_view( '/yith-wpa-addons-container.php', compact( 'addons' ) );
			echo '</div>';
			echo '</div>';
		}

		/**
		 * Save the custom fields
		 *
		 * @param int $product_id Product id.
		 */
		public function yith_wpa_save_fields( $product_id ) {
			$product = wc_get_product( $product_id );

			foreach ( $_POST['yith_wpa_addon'][0] as $key => $addon ) {
				$addons[ $key ] = $addon;
			}

			if ( array_key_exists( '{{INDEX}}', $addons ) ) {
				unset( $addons['{{INDEX}}'] );
			}
//			error_log(print_r('$addons por guardar sin index', true ));
//			error_log(print_r($addons, true ));
			$product->update_meta_data( 'yith_wpa_addons', $addons );
			$product->save();
		}

		/**
		 * Add addon in variations
		 *
		 * @param int     $loop           Position in the loop.
		 * @param array   $variation_data Variation Data.
		 * @param WP_Post $variation      Post data.
		 */
		public function yith_wpa_add_addons_variations( $loop, $variation_data, $variation ) {

			$product_variation = wc_get_product( $variation );

			$addons = $product_variation->get_meta( 'yith_wpa_addons' );

			$addons = is_array( $addons ) ? $addons : array();

			error_log(print_r('$addons variations', true ));
			error_log(print_r($addons, true ));

			echo '<div class="woocommerce_options_panel yith_wpa_variation_addons_options">';
			echo '<div class="options_group">';
			yith_wpa_get_view( '/yith-wpa-addons-container.php', compact( 'addons', 'loop' ) );
			echo '</div>';
			echo '</div>';
		}

		/**
		 * Save addon in product variation
		 *
		 * @param WC_Product_Variation $variation Variation Product.
		 * @param int                  $i         Variation Index.
		 */
		public function yith_wpa_save_addons_variations( $variation, $i ) {
			$i ++;
			if ( ! isset( $_POST['yith_wpa_addon'] ) || ! isset( $_POST['yith_wpa_addon'][ $i ] ) ) {
				return;
			}

			$addons = $_POST['yith_wpa_addon'][ $i ];

			if ( array_key_exists( '{{INDEX}}', $addons ) ) {
				unset( $addons['{{INDEX}}'] );
			}
			error_log(print_r('$addons por guardara var sin index', true ));
			error_log(print_r($addons, true ));
			$variation->update_meta_data( 'yith_wpa_addons', $addons );
			$variation->save();
		}

	}

}
