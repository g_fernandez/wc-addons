<?php
/**
 * Plugin Name: YITH Product Addons
 * Description: Product Addons for YITH Plugins
 * Version: 1.0.0
 * Author: German Fernandez
 * Author URI: https://yithemes.com/
 * Text Domain: yith-plugin-addons
 *
 * @package wc_addons
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! defined( 'YITH_WPA_VERSION' ) ) {
	define( 'YITH_WPA_VERSION', '1.0.0' );
}

if ( ! defined( 'YITH_WPA_DIR_URL' ) ) {
	define( 'YITH_WPA_DIR_URL', plugin_dir_url( __FILE__ ) );
}

if ( ! defined( 'YITH_WPA_DIR_ASSETS_URL' ) ) {
	define( 'YITH_WPA_DIR_ASSETS_URL', YITH_WPA_DIR_URL . 'assets' );
}

if ( ! defined( 'YITH_WPA_DIR_ASSETS_CSS_URL' ) ) {
	define( 'YITH_WPA_DIR_ASSETS_CSS_URL', YITH_WPA_DIR_ASSETS_URL . '/css' );
}

if ( ! defined( 'YITH_WPA_DIR_ASSETS_JS_URL' ) ) {
	define( 'YITH_WPA_DIR_ASSETS_JS_URL', YITH_WPA_DIR_ASSETS_URL . '/js' );
}

if ( ! defined( 'YITH_WPA_DIR_PATH' ) ) {
	define( 'YITH_WPA_DIR_PATH', plugin_dir_path( __FILE__ ) );
}

if ( ! defined( 'YITH_WPA_DIR_INCLUDES_PATH' ) ) {
	define( 'YITH_WPA_DIR_INCLUDES_PATH', YITH_WPA_DIR_PATH . '/includes' );
}

if ( ! defined( 'YITH_WPA_DIR_TEMPLATES_PATH' ) ) {
	define( 'YITH_WPA_DIR_TEMPLATES_PATH', YITH_WPA_DIR_PATH . '/templates' );
}

if ( ! defined( 'YITH_WPA_DIR_VIEWS_PATH' ) ) {
	define( 'YITH_WPA_DIR_VIEWS_PATH', YITH_WPA_DIR_PATH . '/views' );
}


if ( ! function_exists( 'yith_wpa_init_classes' ) ) {

	/**
	 * Yith_wpa_init_classes
	 *
	 * @return void
	 */
	function yith_wpa_init_classes() {

		$i = load_plugin_textdomain( 'yith-plugin-addons', false, basename( dirname( __FILE__ ) ) . '/languages' );
		require_once YITH_WPA_DIR_INCLUDES_PATH . '/class-yith-wpa-plugin-addons.php';
		if ( class_exists( 'YITH_WPA_Plugin_Addons' ) ) {
			yith_wpa_plugin_addons();
		}
	}
}

add_action( 'plugins_loaded', 'yith_wpa_init_classes' );
