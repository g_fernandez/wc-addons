(function($) {

  initAddon       = function ( addonContainer ) {
    addonContainer.find( '.yith_wpa_addon input').each( function() {
      if ( $(this).is('input[type=text]') || $(this).is('textarea') ) {
        $(this).trigger('keyup');
      } else {
        $(this).trigger('change');
      }
    });
  };

  variationChange = function ( event, variation ) {
     console.log($( '.yith_wpa_addons_container > .yith_wpa_addon' ).length);
      var post_data = {
          variation_id: variation.variation_id,
          action: 'yith_wpa_change_variations_addons',
          start_id: $( '.yith_wpa_addons_container > .yith_wpa_addon' ).length,
        },
        productPrice = $('.yith_wpa_product_price .yith_wpa_price_value span');

      yithWcpa.productPrice = variation.display_price;
      product_price = Number.parseFloat(yithWcpa.productPrice).toFixed(yithWcpa.decimalPrecision).toString().replace(".", yithWcpa.decimalSeparator);
      productPrice.html( product_price );

      $.ajax({
        type: "POST",
        dataType: "json",
        url: myAjax.ajaxurl,
        data: post_data,
        success: function (response) {
          //console.log(response.addons);
          var variationAddons = $('.yith_wpa_variation_addons');
          variationAddons.html(response.addons);
          initAddon( variationAddons );
        },
        complete: function () {
          //console.log('complete');
          //calculateAddonsPrice();
        },
      });

  }

  variationClear = function ( event ) {
    $('.yith_wpa_variation_addons').html('');
  }

$( document ).ready( function() {
  $( this ).on( 'found_variation', variationChange );

  $( this ).on( 'reset_data', variationClear );
})

})( jQuery );
