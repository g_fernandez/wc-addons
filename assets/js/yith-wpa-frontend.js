(function($) {

  function calculate( max ) {
      total = 0;
      for ( var i = 0; i < max; i++ ) {
        selector = "[name='yith_wpa_field_" + i + "']";
        fieldType = $( selector ).data( 'field-type' );
        switch ( fieldType ) {
          case 'text':
            val = $( selector ).val();
            price = $( selector ).data( 'price' );
            price_settings = $( selector ).data( 'price-settings' );
            free_chars = $( selector ).data( 'free-chars' );
            if ( 'fixed_price' === price_settings ){
              if ( free_chars < val.length ) {
                total += price;
              }
            } else if ( 'price_per_character' === price_settings ){
              if ( free_chars < val.length ) {
                total += price * ( val.length - free_chars );
              }
            }
            break;
          case 'textarea':
            val = $( selector ).val();
            price = $( selector ).data( 'price' );
            price_settings = $( selector ).data( 'price-settings' );
            free_chars = $( selector ).data( 'free-chars' );
            if ( 'fixed_price' === price_settings ){
              if ( free_chars < val.length ) {
                total += price;
              }
            } else if ( 'price_per_character' === price_settings ){
              if ( free_chars < val.length ) {
                total += price * ( val.length - free_chars );
              }
            }
            break;
          case 'select':
            price = $( selector ).children( "option:selected" ).data( 'price' );
            total += price;
            break;
          case 'radio':
            price = $( selector+":checked" ).data( 'price' );
            total += price;
            break;
          case 'checkbox':
            if ( $( selector ).is( ':checked' ) ) {
              price = $(selector).data('price');
              total += price;
            }
            break;
          case 'onoff':
            if ( $( selector ).is( ':checked' ) ) {
              price = $( selector ).data( 'price' );
              total += price;
            }
            break;
        }
      }

      product_price = Number.parseFloat( yithWcpa.productPrice ).toFixed( yithWcpa.decimalPrecision ).toString().replace( ".", yithWcpa.decimalSeparator );
      $( ".yith_wpa_product_price .yith_wpa_price_value span" ).html( product_price );

      additional_price = Number.parseFloat( total ).toFixed( yithWcpa.decimalPrecision ).toString().replace( ".", yithWcpa.decimalSeparator );
      $( ".yith_wpa_additional_options_price .yith_wpa_price_value span" ).html( additional_price );

      total_price = parseFloat( total ) + parseFloat( yithWcpa.productPrice );
      $( "#yith_wpa_price_totals_hidden" ).val( total_price );

      total_price = Number.parseFloat( total_price ).toFixed( yithWcpa.decimalPrecision ).toString().replace( ".", yithWcpa.decimalSeparator );
      $( ".yith_wpa_total_price .yith_wpa_price_value span" ).html( total_price );


  }

  $( document ).ready( function() {
    max = $( '.yith_wpa_addon' ).length;
    calculate( max );

    $( this ).on( "keyup", ".yith_wpa_addons_text input", function() {
      max = $( '.yith_wpa_addon' ).length;
      calculate( max );
    });

    $( this ).on( "keyup", ".yith_wpa_addons_textarea textarea", function() {
      max = $( '.yith_wpa_addon' ).length;
      calculate( max );
    });

    $( this ).on( "change", ".yith_wpa_addons_select select", function() {
      max = $( '.yith_wpa_addon' ).length;
      calculate( max );
    });

    $( this ).on( "change", ".yith_wpa_addons_radio input", function() {
      max = $( '.yith_wpa_addon' ).length;
      calculate( max );
    });

    $( this ).on( "change", ".yith_wpa_addons_checkbox input", function() {
      max = $( '.yith_wpa_addon' ).length;
      calculate( max );
    });

    $( this ).on( "change", ".yith_wpa_addons_onoff input", function() {
      max = $( '.yith_wpa_addon' ).length;
      calculate( max );
    });

  });

})( jQuery );
