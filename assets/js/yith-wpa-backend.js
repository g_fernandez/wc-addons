var index = 0;
function onInit() {
  (function($) {
    order_index();

    $( ".yith_wpa_addons" ).sortable({
      update: function() {
        order_index();
      }
    });
    $( ".yith_wpa_addons" ).disableSelection();
    $( '.yith_wpa_head_arrow' ).removeClass("dashicons-arrow-up-alt2").addClass("dashicons-arrow-down-alt2");
    $( '.yith_wpa_body' ).hide();
    $(".yith_wpa_addons").children('div.yith_wpa_container').each(function() {

      $(this).html($(this).html().replaceAll("{{INDEX}}", index));
      index++;
      select_val = $(this).find('.yith_wpa_field_type_input select').val();
      selectControl( this, select_val );

    });
  })( jQuery );
}

function selectControl( container, val_field_type ) {
  (function($) {
    if ( "text" === val_field_type || "textarea" === val_field_type ) {
      $(container).find('.yith_wpa_price_per_char').show();
      if ( $(container).find('.yith_wpa_free input').prop('checked') ) {
        $(container).find('.yith_wpa_price_input').hide();
        $(container).find('.yith_wpa_free_characters_input').hide();
      } else {
        $(container).find('.yith_wpa_price_input').show();
        $(container).find('.yith_wpa_free_characters_input').show();
      }
    } else {
      $(container).find('.yith_wpa_price_per_char').hide();
    }

    if ( "select" === val_field_type || "radio" === val_field_type ) {
      $(container).find('.yith_wpa_options_input').show();
      if ( $(container).find('.yith_wpa_free input').prop('checked') ) {
        $(container).find('.yith_wpa_option_container_price').hide();
      } else {
        $(container).find('.yith_wpa_option_container_price').show();
      }
      $(container).find('.yith_wpa_price_input').hide();
      $(container).find('.yith_wpa_free_characters_input').hide();
    } else {
      $(container).find('.yith_wpa_options_input').hide();
    }

    if ( "checkbox" === val_field_type || "onoff" === val_field_type ) {
      $(container).find('.yith_wpa_default_enabled_input').show();
      if ( $(container).find('.yith_wpa_free input').prop('checked') ) {
        $(container).find('.yith_wpa_price_input').hide();
      } else {
        $(container).find('.yith_wpa_price_input').show();
      }
      $(container).find('.yith_wpa_free_characters_input').hide();
    } else {
      $(container).find('.yith_wpa_default_enabled_input').hide();
    }
  })( jQuery );
}

function order_index() {
  (function($) {

    index_hidden = 0;
    $("#product_addons_panel .yith_wpa_addons").children('div.yith_wpa_container').each(function() {
      $(this).find('input[type="hidden"]').val(index_hidden);
      index_hidden++;
    });

    $(".yith_wpa_variation_addons_options .yith_wpa_addons").children('div.yith_wpa_container').each(function() {
      $(this).find('input[type="hidden"]').val(index_hidden);
      index_hidden++;
    });

  })( jQuery );
}

jQuery(document).ready(function($) {

  onInit();

  $(this).on('woocommerce_variations_loaded', function(event) {
    $(".yith_wpa_variation_addons_options").find('.yith_wpa_addons').sortable({
      update: function () {
        order_index();
      }
    });

    $(".yith_wpa_addons").disableSelection();
    $('.yith_wpa_head_arrow').removeClass("dashicons-arrow-up-alt2").addClass("dashicons-arrow-down-alt2");
    $('.yith_wpa_body').hide();
    $(".yith_wpa_addons").children('div.yith_wpa_container').each(function () {

      $(this).html($(this).html().replaceAll("{{INDEX}}", index));
      index++;
      select_val = $(this).find('.yith_wpa_field_type_input select').val();
      selectControl(this, select_val);
    });
  });

  $(this).on( "click", ".yith_wpa_head_arrow", function() {
    if ( $( this ).hasClass( "dashicons-arrow-up-alt2" ) ){
      $( this ).removeClass("dashicons-arrow-up-alt2");
      $( this ).addClass("dashicons-arrow-down-alt2");
      $( this ).closest( '.yith_wpa_container' ).find('.yith_wpa_body').hide();
    } else {
      $( this ).removeClass("dashicons-arrow-down-alt2");
      $( this ).addClass("dashicons-arrow-up-alt2");
      $( this ).closest( '.yith_wpa_container' ).find('.yith_wpa_body').show();
    }
  });

  $(this).on( "click", ".yith_wpa_add_new", function() {
    append = $(this).closest('.yith_wpa_container_total').find('.yith_wpa_addons').first();
    container_clone = $(this).next().find('.yith_wpa_container').first().clone(true).appendTo(append);
    $( container_clone ).html( $( container_clone ).html().replaceAll( '{{INDEX}}', index ) );
    val_field_type = $( container_clone ).find('.yith_wpa_field_type_input select').val();
    selectControl( $( container_clone ), val_field_type );
    index++;
    order_index()
  });

  $(this).on( "click", ".yith_wpa_delete_addon", function() {
    $( this ).closest(".yith_wpa_container").remove();
    order_index()
  });

  $(this).on( "click", ".yith_wpa_add_new_option", function() {
    var clone = $(this).closest(".yith_wpa_options_input").find(".yith_wpa_option_container").first().clone(true).appendTo($(this).closest(".yith_wpa_options_input").find(".yith_wpa_option_container_total"));
    clone.find('span.yith_wpa_option_delete').removeAttr( "style" );
    clone.find('input.yith_wpa_option_name').removeAttr("value");
    clone.find('input.yith_wpa_option_price').removeAttr("value");
    clone.find('input.yith_wpa_option_name').val("");
    clone.find('input.yith_wpa_option_price').val("");
  });

  $(this).on( "click", "span.yith_wpa_option_delete", function() {
    $( this ).closest(".yith_wpa_option_container").remove();
  });

  $(this).on( "keyup", ".yith_wpa_name_input input", function() {
    valor = $(this).val();
    $(this).closest('.yith_wpa_container').find('span.yith_wpa_head_name').html(valor);

  });

  $(this).on( "change", ".yith_wpa_field_type_input select", function() {
    valor = $(this).val();
    container = $( this ).closest('.yith_wpa_container');
    selectControl( container, valor );
  });

  $(this).on( "change", ".yith_wpa_price_settings_input input", function() {
    select_val = $(this).closest('.yith_wpa_container').find('.yith_wpa_field_type_input select').val();
    switch($(this).val()) {
      case 'free':
        $(this).closest('.yith_wpa_container').find('.yith_wpa_price_input').hide();
        $(this).closest('.yith_wpa_container').find('.yith_wpa_free_characters_input').hide();
        $(this).closest('.yith_wpa_container').find('.yith_wpa_option_container_price').hide();
        break;
      case 'fixed_price':
        if ( "select" === select_val || "radio" === select_val ){
          $(this).closest('.yith_wpa_container').find('.yith_wpa_price_input').hide();
          $(this).closest('.yith_wpa_container').find('.yith_wpa_free_characters_input').hide();
          $(this).closest('.yith_wpa_container').find('.yith_wpa_option_container_price').show();
        } else if ( "text" === select_val || "textarea" === select_val ){
          $(this).closest('.yith_wpa_container').find('.yith_wpa_price_input').show();
          $(this).closest('.yith_wpa_container').find('.yith_wpa_free_characters_input').show();
        } else {
          $(this).closest('.yith_wpa_container').find('.yith_wpa_price_input').show();
          $(this).closest('.yith_wpa_container').find('.yith_wpa_free_characters_input').hide();
        }
        break;
      case 'price_per_character':
        $(this).closest('.yith_wpa_container').find('.yith_wpa_price_input').show();
        $(this).closest('.yith_wpa_container').find('.yith_wpa_free_characters_input').show();
        break;
    }
  });

});
