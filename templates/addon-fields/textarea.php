<?php
?>
<div class="yith_wpa_addons_textarea">
	<h3 class="yith_wpa_display_textarea_name"><?php echo esc_attr( $addon['name'] ); ?></h3>
	<p><?php echo esc_attr( $addon['description'] ); ?></p>

	<?php
	if ( isset( $addon['price'] ) && 'free' !== $addon['price_settings'] ) {
		?>
		<div class="yith_wpa_display_price">
			<p>+<?php echo wp_kses_post( wc_price( (float) $addon['price'] ) ); ?></p>
		</div>
		<?php
	}
	?>

	<textarea
		name="yith_wpa_field_<?php echo esc_attr( $addon['index'] ); ?>"
		id="yith_wpa_field_<?php echo esc_attr( $addon['index'] ); ?>"
		data-field-type="<?php echo esc_attr( $addon['field_type'] ); ?>"
		data-price="<?php echo 'free' !== $addon['price_settings'] ? esc_attr( $addon['price'] ) : 0; ?>"
		data-price-settings="<?php echo esc_attr( $addon['price_settings'] ); ?>"
		data-free-chars="<?php echo 'free' !== $addon['price_settings'] ? esc_attr( $addon['free_characters'] ) : 0; ?>"
	></textarea>

</div>
