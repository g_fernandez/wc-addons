<?php
?>
<div class="yith_wpa_addons_select">
	<h3><?php echo esc_attr( $addon['name'] ); ?></h3>

	<p><?php echo esc_attr( $addon['description'] ); ?></p>

	<select class="yith-proteo-standard-select yith_wpa_display_select" name="yith_wpa_field_<?php echo esc_attr( $addon['index'] ); ?>" id="yith_wpa_field_<?php echo esc_attr( $addon['index'] ); ?>" data-field-type="<?php echo esc_attr( $addon['field_type'] ); ?>">
		<?php
		foreach ( $addon['options']['name'] as $key => $option_name ) {
			?>
			<option value="<?php echo esc_attr( $option_name ); ?>"<?php echo 0 === $key ? 'selected' : ''; ?> data-price="<?php echo 'free' !== $addon['price_settings'] ? esc_attr( $addon['options']['price'][ $key ] ) : 0; ?>">
				<?php echo esc_attr( $option_name ); ?><span> <?php echo 'free' !== $addon['price_settings'] ? '+' . wp_kses_post( wc_price( (float) $addon['options']['price'][ $key ] ) ) : ''; ?></span>
			</option>
			<?php
		}
		?>
	</select>

</div>
