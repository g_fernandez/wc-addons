<?php
?>
<div class="yith_wpa_addons_radio">
	<h3><?php echo esc_attr( $addon['name'] ); ?></h3>

	<p><?php echo esc_attr( $addon['description'] ); ?></p>

	<div class="yith_wpa_radio_container">
		<?php
		foreach ( $addon['options']['name'] as $key => $option_name ) {
			?>
			<div class="yith_wpa_radio_options_container">
				<input
					type="radio"
					name="yith_wpa_field_<?php echo esc_attr( $addon['index'] ); ?>"
					class="yith-proteo-standard-radio"
					data-field-type="<?php echo esc_attr( $addon['field_type'] ); ?>"
					data-price="<?php echo 'free' !== $addon['price_settings'] ? esc_attr( $addon['options']['price'][ $key ] ) : 0; ?>"
					value="<?php echo esc_attr( $option_name ); ?>"
					<?php checked( $key, 0 ); ?>
				>
				<label for="yith_wpa_addons_radio_<?php echo esc_attr( $addon['index'] ); ?>"><?php echo esc_attr( $option_name ); ?>  <span> <?php echo 'free' !== $addon['price_settings'] ? '+' . wp_kses_post( wc_price( (float) $addon['options']['price'][ $key ] ) ) : ''; ?></span></label>
			</div>
			<?php
		}
		?>
	</div>

</div>
