<?php
?>
<div class="yith_wpa_addons_checkbox">
	<h3><?php echo esc_attr( $addon['name'] ); ?></h3>
	<div class="yith_wpa_checkbox_container">
		<input
			type="checkbox"
			name="yith_wpa_field_<?php echo esc_attr( $addon['index'] ); ?>"
			id="yith_wpa_field_<?php echo esc_attr( $addon['index'] ); ?>"
			data-field-type="<?php echo esc_attr( $addon['field_type'] ); ?>"
			data-price="<?php echo 'free' !== $addon['price_settings'] ? esc_attr( $addon['price'] ) : 0; ?>"
			<?php
			if ( isset( $addon['default_enabled'] ) ) {
				checked( $addon['default_enabled'], 'yes' );
			}
			?>
		>
		<p class="yith_wpa_checkbox_description"><?php echo esc_attr( $addon['description'] ); ?></p>
	</div>
	<?php
	if ( isset( $addon['price'] ) && 'free' !== $addon['price_settings'] ) {
		?>
		<div class="yith_wpa_display_price">
			<p>+<?php echo wp_kses_post( wc_price( (float) $addon['price'] ) ); ?></p>
		</div>
		<?php
	}
	?>
</div>
