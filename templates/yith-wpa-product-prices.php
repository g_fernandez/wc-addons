<?php
?>
<div class="yith_wpa_price_totals_container">
	<h3>Price Totals</h3>
	<input type="hidden" name="yith_wpa_price_totals_hidden" id="yith_wpa_price_totals_hidden">
	<div class="yith_wpa_price_totals">
		<div class="yith_wpa_product_price" data-product-price="<?php echo esc_attr( $product_price ); ?>">
			<p class="yith_wpa_title"><?php esc_html_e( 'Product price', 'yith-plugin-addons' ); ?></p>
			<p class="yith_wpa_price_value price"><?php echo esc_attr( $currency ); ?><span><?php echo esc_attr( $product_price ); ?></span></p>
		</div>
		<div class="yith_wpa_additional_options_price">
			<p class="yith_wpa_title"><?php esc_html_e( 'Additional options total', 'yith-plugin-addons' ); ?></p>
			<p class="yith_wpa_price_value price"><?php echo esc_attr( $currency ); ?><span></span></p>
		</div>
		<div class="yith_wpa_total_price">
			<p class="yith_wpa_title"><?php esc_html_e( 'Total', 'yith-plugin-addons' ); ?></p>
			<p class="yith_wpa_price_value price"><?php echo esc_attr( $currency ); ?><span></span></p>
		</div>
	</div>
</div>
